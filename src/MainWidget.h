#pragma once

#include <QtWidgets/QWidget>
#include "WorkSpace.h"
#include "ui_TabWidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class TabWidgetClass; };
QT_END_NAMESPACE

class TabWidget : public QWidget
{ 
    Q_OBJECT

public:
    TabWidget(QWidget *parent = nullptr);
    ~TabWidget();

    void openFile();

private:
    Ui::TabWidgetClass *ui;
    WorkSpace* wSpace;
};
