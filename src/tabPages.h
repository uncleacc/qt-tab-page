#pragma once
#include <QMenu>
#include <QTabWidget>
#include <QDropEvent>
#include <QDragMoveEvent>
#include <QDragEnterEvent>
#include "mySplitter.h"
#include "myTabBar.h"

class tabPages : public QTabWidget
{ 
	Q_OBJECT
public:
	tabPages(QWidget* parent = nullptr);
	~tabPages();

	void openFile(const QString& filePath);
	void updateActionStatus();
	void updateActionStatus(bool status);
	void createSplitter(mySplitter* parent, mySplitter* child, int index);
	void moveTab(tabPages* src, tabPages* dest);
	void insTab(tabPages* src, tabPages* dest, int index);
	void reAdjust(tabPages* originalTab);
	void curWindowChanged(tabPages* changed);
	void addWidget(mySplitter* container, tabPages* child);
	void addWidget(mySplitter* container, mySplitter* child);

	void dragEnterEvent(QDragEnterEvent* event) override;
	void dropEvent(QDropEvent* event) override;
	void dragMoveEvent(QDragMoveEvent* event) override;
	void dragLeaveEvent(QDragLeaveEvent* event) override;
	void paintEvent(QPaintEvent* event) override;
	void resizeEvent(QResizeEvent* event) override;
	//bool eventFilter(QObject* object, QEvent* event) override;

public slots:
	//override
	
	//slots
	void slot_deleteTab(int index);
	void slot_onMenuShow(const QPoint& pos);
	void slot_toRight();
	void slot_toDown();
	void slot_toLeft();
	void slot_toUp();
	void slot_curWindowChanged();
	void slot_dragTab();
	void slot_dragTab(QObject* src, int index);
	void slot_manify(int);

private:
	myTabBar* ctoTabBar;
	QMenu* tabMenu;
	QAction* toRight;
	QAction* toDown;
	QAction* toLeft;
	QAction* toUp;
	QAction* fullScreen;
	QWidget* masker;

signals:
	void manify(modelWidget*);
};

