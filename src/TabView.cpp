#include "TabView.h"

TabView::TabView(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::TabViewClass())
{
    ui->setupUi(this);
}

TabView::~TabView()
{
    delete ui;
}
