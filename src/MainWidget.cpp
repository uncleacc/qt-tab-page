#include "MainWidget.h"
#include "WorkSpace.h"
#include <QHBoxLayout>
#include <QFileDialog>
#include <QMessageBox>

TabWidget::TabWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::TabWidgetClass())
{
    ui->setupUi(this);

    wSpace = new WorkSpace(ui->widget);

    QHBoxLayout* layout = new QHBoxLayout(ui->widget); 
    layout->addWidget(wSpace);
    //layout->setContentsMargins(0, 0, 0, 0);
    ui->widget->setLayout(layout);

    connect(ui->btnOpen, &QPushButton::clicked, this, &TabWidget::openFile);
}

TabWidget::~TabWidget()
{
    delete ui;
}

void TabWidget::openFile()
{
    QString filePath = QFileDialog::getOpenFileName(this, "choose file", "C:\\Users\\Administrator\\Pictures", "images(*.png *.jpg)");
    if (filePath.isEmpty()) {
        return;
    }
    bool isOk = wSpace->OpenFile(filePath);
    if (!isOk) {
        QMessageBox::critical(this, "Error", "Invalid file path.", QMessageBox::Ok, QMessageBox::NoButton);
        return;
    }
}
