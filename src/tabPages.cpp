#include "tabPages.h"
#include <QDebug>
#include <QDrag>
#include <QList>
#include <QPainter>
#include <QMimeData>
#include <QToolTip>
#include "mySplitter.h"
#include "WorkSpace.h"
#include "modelWidget.h"
#include "myTabBar.h"
#include "resizeWidget.h"

tabPages::tabPages(QWidget* parent) : QTabWidget(parent)
{
	ctoTabBar = new myTabBar;
	masker = new QWidget(this);
	masker->setVisible(false);
	setTabBar(ctoTabBar);
	setContextMenuPolicy(Qt::CustomContextMenu);
	setAcceptDrops(true);

	tabMenu = new QMenu(this);

	toRight = new QAction("to right", tabMenu);
	toRight->setEnabled(false);
	tabMenu->addAction(toRight);
	connect(toRight, &QAction::triggered, this, &tabPages::slot_toRight);

	toDown = new QAction("to Down", tabMenu);
	toDown->setEnabled(false);
	tabMenu->addAction(toDown);
	connect(toDown, &QAction::triggered, this, &tabPages::slot_toDown);

	toLeft = new QAction("to Left", tabMenu);
	toLeft->setEnabled(false);
	tabMenu->addAction(toLeft);
	connect(toLeft, &QAction::triggered, this, &tabPages::slot_toLeft);

	toUp = new QAction("to Up", tabMenu);
	toUp->setEnabled(false);
	tabMenu->addAction(toUp);
	connect(toUp, &QAction::triggered, this, &tabPages::slot_toUp);

	fullScreen = new QAction("full Screen", tabMenu);
	fullScreen->setEnabled(true);
	tabMenu->addAction(fullScreen);
	connect(fullScreen, &QAction::triggered, this, &tabPages::slot_manify);
	/*QAction* closeCur = new QAction("close all files", tabMenu);
	tabMenu->addAction(closeCur);
	connect(closeCur, SIGNAL(triggered()), this, SLOT(slot_closeCurrentTab()));

	QAction* closeAllExceptMe = new QAction(QString("close all non-current files"), tabMenu);
	tabMenu->addAction(closeAllExceptMe);
	connect(closeAllExceptMe, SIGNAL(triggered()), this, SLOT(slot_closeIndexofThis()));

	QAction* closeLeft = new QAction(QString("close left all"), tabMenu);
	tabMenu->addAction(closeLeft);
	connect(closeLeft, SIGNAL(triggered()), this, SLOT(slot_closeLeftAll()));

	QAction* closeRight = new QAction(QString("close all on the right"), tabMenu);
	tabMenu->addAction(closeRight);
	connect(closeRight, SIGNAL(triggered()), this, SLOT(slot_closeRightAll()));*/
	connect(this, &QTabWidget::tabCloseRequested, this, &tabPages::slot_deleteTab); //delete tab
	connect(this, &QTabWidget::customContextMenuRequested, this, &tabPages::slot_onMenuShow); //show menu
	connect(this, &QTabWidget::tabBarClicked, this, &tabPages::slot_curWindowChanged); //click tab
	connect(ctoTabBar, SIGNAL(dragTabRequest()), this, SLOT(slot_dragTab()));
	connect(ctoTabBar, SIGNAL(dragTabRequest(QObject*, int)), this, SLOT(slot_dragTab(QObject*, int)));
}

tabPages::~tabPages()
{
}

void tabPages::openFile(const QString& filePath)
{
	modelWidget* newTab = new modelWidget;
	newTab->openFile(filePath);
	resizeWidget* wrapTab = new resizeWidget(curTabWindow, newTab);
	curTabWindow->addTab((QWidget*)wrapTab, "tab");
	curTabWindow->resize(curTabWindow->size() - QSize(1, 1)); //触发resize调整Tab宽度
	curTabWindow->resize(curTabWindow->size() + QSize(1, 1));
	curTabWindow->setCurrentIndex(curTabWindow->count() - 1);
	curTabWindow->show();
	curWindowChanged(this);
}

void tabPages::updateActionStatus() {
	if (this->count() > 1) {
		updateActionStatus(true);
	}
	else {
		updateActionStatus(false);
	}
}

void tabPages::updateActionStatus(bool status) {
	toRight->setEnabled(status);
	toDown->setEnabled(status);
	toLeft->setEnabled(status);
	toUp->setEnabled(status);
}

void tabPages::createSplitter(mySplitter* parent, mySplitter* child, int index)
{
	parent->insertWidget(index, child);
	parent->setBottom(false);
}

void tabPages::moveTab(tabPages* src, tabPages* dest)
{
	dest->addTab(src->currentWidget(), src->tabText(src->currentIndex()));
	resize(size() - QSize(1, 1)); //触发resize调整Tab宽度
	resize(size() + QSize(1, 1));
	curWindowChanged(dest);
}

void tabPages::insTab(tabPages* src, tabPages* dest, int index)
{
	dest->insertTab(index, src->currentWidget(), src->tabText(src->currentIndex()));
}

void tabPages::dragEnterEvent(QDragEnterEvent* event)
{
	event->accept();
}

void tabPages::dragMoveEvent(QDragMoveEvent* event) {
	QPoint cursor = event->pos();
	int x = pos().x(), y = pos().y() + ctoTabBar->height();
	int w = width(), h = height() - ctoTabBar->height();
	QRect left(x, y, w / 4, h);
	QRect right(x + w / 4 * 3, y, w / 4, h);
	QRect up(x, y, w, h / 4);
	QRect down(x, y + h / 4 * 3, w, h / 4);
	QRect center(x + w / 4, y + h / 4, w / 2, h / 2);
	QRect res;
	if (left.contains(cursor)) {
		res.setTopLeft(QPoint(x, y));
		res.setSize(QSize(w / 2, h));
	}
	else if (right.contains(cursor)) {
		res.setTopLeft(QPoint(x + w / 2, y));
		res.setSize(QSize(w / 2, h));
	}
	else if (up.contains(cursor)) {
		res.setTopLeft(QPoint(x, y));
		res.setSize(QSize(w, h / 2));
	}
	else if (down.contains(cursor)) {
		res.setTopLeft(QPoint(x, y + h / 2));
		res.setSize(QSize(w, h / 2));
	}
	else if (center.contains(cursor)) {
		res.setTopLeft(QPoint(x, y));
		res.setSize(QSize(w, h));
	}
	masker->resize(res.width(), res.height());
	masker->move(res.topLeft());
	masker->setVisible(true);
	masker->raise();
	masker->setStyleSheet("background-color: rgba(0, 0, 150, 100);");
	event->accept();
}

void tabPages::dragLeaveEvent(QDragLeaveEvent* event)
{
	masker->setVisible(false);
	event->accept();
}

void tabPages::dropEvent(QDropEvent* event)
{
	masker->setVisible(false);
	if (event->source() != this) {
		moveTab((tabPages*)event->source(), this);
		setCurrentIndex(count() - 1);
	}
	else if (count() == 1) {
		event->ignore();
		return;
	}
	QPoint cursor = event->pos();
	int x = this->pos().x(), y = this->pos().y();
	int w = this->width(), h = this->height();
	QRect left(x, y, w / 4, h);
	QRect right(x + w / 4 * 3, y, w / 4, h);
	QRect up(x, y, w, h / 4);
	QRect down(x, y + h / 4 * 3, w, h / 4);
	QRect center(x + w / 4, y + h / 4, w / 2, h / 2);
	if (left.contains(cursor)) {
		slot_toLeft();
	}
	else if (right.contains(cursor)) {
		slot_toRight();
	}
	else if (up.contains(cursor)) {
		slot_toUp();
	}
	else if (down.contains(cursor)) {
		slot_toDown();
	}
	event->accept();
}

//slots
void tabPages::slot_onMenuShow(const QPoint& pos)
{
	updateActionStatus();
	QPoint c_pos = QPoint(pos.x(), pos.y());
	int index = this->tabBar()->tabAt(c_pos);
	this->setCurrentIndex(index);
	if (index != -1)
	{
		tabMenu->exec(QCursor::pos());
	}
}

void tabPages::addWidget(mySplitter* container, tabPages* child) {
	disconnect(child, &tabPages::manify, 0, 0);
	container->addWidget(child);
	child->setParent(container);
	connect(child, &tabPages::manify, container, &mySplitter::manify);
}

void tabPages::addWidget(mySplitter* container, mySplitter* child) {
	disconnect(child, &mySplitter::manify, 0, 0);
	container->addWidget(child);
	container->addChild(child); //Maintain parent-child lists
	child->setParent(container);
	connect(child, &mySplitter::manify, container, &mySplitter::manify);
}

void tabPages::slot_toRight()
{
	mySplitter* parent = (mySplitter*)this->parent(); //is not bottom
	parent->setOrientation(Qt::Horizontal);

	int oldIndex = parent->indexOf(this);

	mySplitter* old = new mySplitter(parent, true); //is bottom
	addWidget(old, this);
	//old->addWidget(this);
	//this->setParent(old);
	//connect(this, tabPages::manify, old, mySplitter::manify);

	mySplitter* New = new mySplitter(parent, true); //is bottom
	tabPages* newTab = new tabPages(New);
	addWidget(New, newTab);
	//New->addWidget(newTab);
	//connect(this, tabPages::manify, old, mySplitter::manify);
	
	moveTab(this, newTab); //move old tab to new tab
		
	if (oldIndex != -1) {
		createSplitter(parent, old, oldIndex);
		createSplitter(parent, New, oldIndex + 1);
	}

	old->show();
	New->show();
	this->show();
}

void tabPages::slot_toDown()
{
	mySplitter* parent = (mySplitter*)this->parent();
	parent->setOrientation(Qt::Vertical);
	parent->setBottom(false);

	int oldIndex = parent->indexOf(this);

	mySplitter* old = new mySplitter(parent, true);
	addWidget(old, this);
	//old->addWidget(this);
	//this->setParent(old);

	mySplitter* New = new mySplitter(parent, true);
	tabPages* newTab = new tabPages(New);
	addWidget(New, newTab);
	
	moveTab(this, newTab); //move old tab to new tab
	
	if (oldIndex != -1) {
		createSplitter(parent, old, oldIndex);
		createSplitter(parent, New, oldIndex + 1);
	}
	old->show();
	New->show();
	this->show();
}

void tabPages::slot_toLeft()
{
	mySplitter* parent = (mySplitter*)this->parent();
	parent->setOrientation(Qt::Horizontal);
	parent->setBottom(false);

	int oldIndex = parent->indexOf(this);

	mySplitter* old = new mySplitter(parent, true); //set old splitter
	addWidget(old, this);
	//old->addWidget(this);
	//this->setParent(old);

	mySplitter* New = new mySplitter(parent, true); //set new splitter
	tabPages* newTab = new tabPages(New);
	addWidget(New, newTab);
	
	moveTab(this, newTab); //move old tab to new tab
	
	if (oldIndex != -1) {
		createSplitter(parent, old, oldIndex);
		createSplitter(parent, New, oldIndex);
	}

	old->show();
	New->show();
	this->show();
}

void tabPages::slot_toUp()
{
	mySplitter* parent = (mySplitter*)this->parent();
	parent->setOrientation(Qt::Vertical);
	parent->setBottom(false);

	int oldIndex = parent->indexOf(this);

	mySplitter* old = new mySplitter(parent, true);
	addWidget(old, this);
	//old->addWidget(this);
	//this->setParent(old);

	mySplitter* New = new mySplitter(parent, true);
	tabPages* newTab = new tabPages(New);
	addWidget(New, newTab);

	moveTab(this, newTab); //move old tab to new tab

	if (oldIndex != -1) {
		createSplitter(parent, old, oldIndex);
		createSplitter(parent, New, oldIndex);
	}

	old->show();
	New->show();
	this->show();
}

void tabPages::slot_curWindowChanged()
{
	curWindowChanged(this);	
	//解决奇怪的bug：modelwidget的尺寸会超过resizeWidget的尺寸
	resize(size() - QSize(1, 1)); //触发resize调整Tab宽度
	resize(size() + QSize(1, 1));
}

void tabPages::curWindowChanged(tabPages* changed) {
	if (curTabWindow) {
		curTabWindow->setStyleSheet("QTabBar::tab:selected{ background-color: rgb(228, 233, 242); padding: 2px;}");
	}
	curTabWindow = changed;
	curTabWindow->setStyleSheet("QTabBar::tab:selected{ background-color:rgb(104, 191, 249); border: 1px solid blue;}");
}

void tabPages::slot_dragTab()
{
	QDrag* drag = new QDrag(this);
	QMimeData* md = new QMimeData;
	md->setText("");
	drag->setMimeData(md);
	drag->setPixmap(grab(tabBar()->tabRect(currentIndex())));
	drag->exec(Qt::MoveAction);
	reAdjust(this);
}

void tabPages::slot_manify(int)
{
	int t = curTabWindow->count();
	emit manify((modelWidget*)currentWidget());
}

void tabPages::slot_dragTab(QObject* src, int index)
{
	insTab((tabPages*)src, this, index);
	this->setCurrentIndex(index);
	resize(size() - QSize(1, 1)); //触发resize调整Tab宽度
	resize(size() + QSize(1, 1));
}

void tabPages::paintEvent(QPaintEvent* event)
{
	//int x = rect().topLeft().x(), y = rect().topLeft().y();
	//int w = rect().width(), h = rect().height();
	//QColor mask(0, 0, 255, 100);
	//QPainter painter(this);
	//if (onLeft) {
	//	painter.fillRect(x, y, w / 2, h, mask);  // 上半区域绘制蓝色蒙版
	//}
	//else if (onRight) {
	//	painter.fillRect(x + w / 2, y, w / 2, h, mask);  // 上半区域绘制蓝色蒙版
	//}
	//else if (onUp) {
	//	painter.fillRect(x, y, w, h / 2, mask);
	//}
	//else if (onDown) {
	//	painter.fillRect(x, y + h / 2, w, h / 2, mask);
	//}
	//else if (onCenter) {
	//	painter.fillRect(x, y, w, h, mask);
	//}
	QPainter p(this);
	QPoint Pos(pos().x(), pos().y() + tabBar()->height());
	p.fillRect(QRect(Pos, size()), Qt::gray);
	event->accept();
}

void tabPages::resizeEvent(QResizeEvent* event)
{
	tabBar()->setFixedWidth(std::min(width(), count() * 100));
	QTabWidget::resizeEvent(event);
}

void tabPages::slot_deleteTab(int index)
{
	QWidget* target = widget(index);
	removeTab(index);
	delete target;
	reAdjust(this);
	resize(size() - QSize(1, 1)); //触发resize调整Tab宽度
	resize(size() + QSize(1, 1));
	return ;
}

void tabPages::reAdjust(tabPages* originalTab) {
	if (originalTab->count() == 0) { //this tab widget do not contain any page, delete the tab widget
		mySplitter* curSplitter = (mySplitter*)(originalTab->parent()); // get current Splitter
		if (!curSplitter->isTop()) {
			mySplitter* Father = dynamic_cast<mySplitter*>(curSplitter->parent());
			if (curTabWindow == originalTab) {
				curTabWindow = nullptr;
			}
			Father->deleteChild(curSplitter);
			//when father's child(mySplitter) number is equal to 1, we need to readjust structure to prevent useless nesting
			if (Father->getChildNum() == 1) {
				mySplitter* brother = Father->getChild(0);
				if (brother->isBottom()) { //the son is QTabWidget
					tabPages* content = dynamic_cast<tabPages*>(brother->widget(0));
					Father->setBottom(true); //father is yeaf
					addWidget(Father, content);
					curWindowChanged(content);
				}
				else { //the sons are QSplitters
					int cnt = brother->count();
					Father->setOrientation(brother->orientation());
					for (int i = 0; i < cnt; i++) {
						mySplitter* content = dynamic_cast<mySplitter*>(brother->widget(0));
						addWidget(Father, content);
					}
				}
				Father->deleteChild(brother);
			}
			else {
				curWindowChanged((tabPages*)Father->widget(0));
			}
		}
	}
}

//bool tabPages::eventFilter(QObject* object, QEvent* event) {
//	if (object == currentWidget() && draging) {
//		if (event->type() == QEvent::Paint) {
//			return true;
//		}
//	}
//	return false;
//}