#pragma once
#include <QWidget>

class resizeWidget: public QWidget
{
public:
    resizeWidget(QWidget* parent = nullptr, QWidget* child = nullptr);
    ~resizeWidget();

    void resizeEvent(QResizeEvent* event) override;
};

