#pragma once

#include <QtWidgets/QWidget>
#include "ui_TabView.h"

QT_BEGIN_NAMESPACE
namespace Ui { class TabViewClass; };
QT_END_NAMESPACE

class TabView : public QWidget
{
    Q_OBJECT

public:
    TabView(QWidget *parent = nullptr);
    ~TabView();

private:
    Ui::TabViewClass *ui;
};
