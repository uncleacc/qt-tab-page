#include "modelWidget.h"
#include "tabPages.h"
#include "resizeWidget.h"
#include <QPainter>
#include <QDebug>

modelWidget::modelWidget(QWidget* parent) : QWidget(parent)
{
	setMinimumHeight(100);
	setMinimumWidth(100);
}

modelWidget::~modelWidget()
{
}

void modelWidget::openFile(const QString& filePath)
{
	this->filePath = filePath;
}

void modelWidget::paintEvent(QPaintEvent* event)
{ 
	QPixmap pixmap = QPixmap(filePath).scaled(this->size());
	QPainter painter(this);
	painter.drawPixmap(this->rect(), pixmap);
}

//拉伸窗口后切换tab，resizeWidget中的布局就不起作用了，modelWidget宽度超过其父亲的宽度，图片显示不全，不知道问题所在
//只能重写resizeEvent函数
void modelWidget::resizeEvent(QResizeEvent* event)
{
	resizeWidget* father = dynamic_cast<resizeWidget*>(parent());
	if (father != nullptr) {
		resize(father->size());
	}
	
	QWidget::resizeEvent(event);
}

