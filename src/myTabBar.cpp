#include "myTabBar.h"
#include <QDebug>

myTabBar::myTabBar(QWidget* parent): QTabBar(parent)
{
    setTabsClosable(true);
    //setShape(QTabBar::TriangularNorth);
    //setMovable(true);
    setAcceptDrops(true);
}

myTabBar::~myTabBar()
{
}

void myTabBar::mouseMoveEvent(QMouseEvent* event)
{
    if (event->buttons() == Qt::LeftButton) {
        if (!tabRect(currentIndex()).contains(event->pos())) {
            emit dragTabRequest();
            return;
        }
    }
    QTabBar::mouseMoveEvent(event);
}

void myTabBar::dragEnterEvent(QDragEnterEvent* event)
{
    event->accept();
}

void myTabBar::dragMoveEvent(QDragMoveEvent* event) {
    event->accept();
}

void myTabBar::dragLeaveEvent(QDragLeaveEvent* event) {
    event->accept();
}

void myTabBar::dropEvent(QDropEvent* event)
{
    if (event->source() == this) {
        event->ignore();
    }
    else {
        int index = tabAt(event->pos());
        if (index != -1) {
            emit dragTabRequest(event->source(), index);
        }
        event->accept();
    }
}
