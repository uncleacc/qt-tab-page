#include "mySplitter.h"
#include "config.h"
#include <QDebug>

mySplitter::mySplitter(QWidget* parent, bool bottom, bool top)
{
	if (!top) {
		father = dynamic_cast<mySplitter*>(parent);
		father->addChild(this);
		connect(this, &mySplitter::manify, this, &mySplitter::test);
		connect(this, &mySplitter::manify, (mySplitter*)parent, &mySplitter::manify);
	}
	setTop(top);
	setBottom(bottom);
	setChildrenCollapsible(false); 
}

mySplitter::~mySplitter()
{
}

bool mySplitter::isTop() const
{
	return Top;
}

bool mySplitter::isBottom() const
{
	return Bottom;
}

void mySplitter::setTop(bool top)
{
	Top = top;
	return;
}

void mySplitter::setBottom(bool bottom)
{
	Bottom = bottom;
	return ;
}

void mySplitter::addChild(mySplitter* child)
{
	children.push_back(child);
}

void mySplitter::deleteChild(mySplitter* child)
{
	int index = children.indexOf(child);
	children.remove(index);
	if(child) delete child;
}

int mySplitter::getChildNum() const
{
	return children.size();
}

mySplitter* mySplitter::getChild(int index)
{
	return children[index];
}

void mySplitter::test() {
	qDebug() << "s";
}