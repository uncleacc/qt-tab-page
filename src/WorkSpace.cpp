#include <QImage>
#include <QPalette>
#include <QMessageBox>
#include "tabPages.h"
#include "WorkSpace.h"
#include "modelWidget.h"


WorkSpace::WorkSpace(QWidget* parent): QWidget(parent)
{	
	//setStyleSheet("border: 3px solid");
	stkWidget = new QStackedWidget(this);
	container = new mySplitter(stkWidget, true, true);
	stkWidget->addWidget(container);
	whiteBoard = new QWidget(stkWidget);
	whiteBoard_layout = new QVBoxLayout(whiteBoard);
	closeBtn = new QPushButton("close", whiteBoard); 
	whiteBoard_layout->addWidget(closeBtn);
	whiteBoard->setLayout(whiteBoard_layout);
	whiteBoard_layout->setContentsMargins(0, 0, 0, 0);
	stkWidget->addWidget(whiteBoard);
	curTabWindow = new tabPages(container);

	connect(curTabWindow, &tabPages::manify, container, &mySplitter::manify);

	QHBoxLayout* layout = new QHBoxLayout(this);
	layout->addWidget(stkWidget);
	layout->setContentsMargins(0, 0, 0, 0);
	setLayout(layout);

	connect(container, &mySplitter::manify, this, &WorkSpace::slot_FullScreen);
	connect(closeBtn, &QPushButton::clicked, this, &WorkSpace::slot_restore);
}

WorkSpace::~WorkSpace()
{
}

bool WorkSpace::OpenFile(const QString& filePath) const
{
	QImage image;
	if (!image.load(filePath)) {
		return false;
	}
	//file is load successfully
	curTabWindow->openFile(filePath);
	return true;
}

void WorkSpace::slot_FullScreen(modelWidget* src)
{
	oriIndex = curTabWindow->currentIndex();
	whiteBoard_layout->addWidget(curTabWindow->currentWidget());
	stkWidget->setCurrentIndex(1);
	src->show();
}

void WorkSpace::slot_restore()
{
	modelWidget* model;
	if (whiteBoard_layout->count() == 2) {
		model = (modelWidget*)whiteBoard_layout->takeAt(1)->widget();
		whiteBoard_layout->removeWidget(model);
	}
	curTabWindow->insertTab(oriIndex, model, "tab");
	curTabWindow->setCurrentIndex(oriIndex);
	stkWidget->setCurrentIndex(0);
}
