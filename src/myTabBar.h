#pragma once
#include <QObject>
#include <QTabBar>
#include <QMouseEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDragLeaveEvent>

class myTabBar : public QTabBar 
{
    Q_OBJECT
public:
    myTabBar(QWidget* parent = nullptr);
    ~myTabBar();

    //override
    void mouseMoveEvent(QMouseEvent* event) override;
    void dropEvent(QDropEvent* event) override;
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dragMoveEvent(QDragMoveEvent* event) override;
    void dragLeaveEvent(QDragLeaveEvent* event) override;

signals:
    void dragTabRequest();
    void dragTabRequest(QObject* src, int index);

};

