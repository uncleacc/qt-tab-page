#pragma once
#include <QSplitter>
#include <QWidget>
#include <QMimeData>
#include <QDragEnterEvent>
#include "modelWidget.h"

class mySplitter : public QSplitter
{
	Q_OBJECT 
public:
	mySplitter(QWidget* parent = nullptr, bool bottom = false, bool top = false);
	~mySplitter();

	bool isTop()const;
	bool isBottom()const;
	void setTop(bool top);
	void setBottom(bool bottom);
	void addChild(mySplitter* child = nullptr);
	void deleteChild(mySplitter* child = nullptr);
	int getChildNum()const;
	mySplitter* getChild(int index);

	void test();

private:
	mySplitter* father;
	QVector<mySplitter*> children;
	bool Top;
	bool Bottom;

signals:
	void manify(modelWidget*);
};

