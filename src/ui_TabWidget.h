/********************************************************************************
** Form generated from reading UI file 'TabWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABWIDGET_H
#define UI_TABWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TabWidgetClass
{
public:
    QVBoxLayout *verticalLayout_2;
    QPushButton *btnOpen;
    QWidget *widget;

    void setupUi(QWidget *TabWidgetClass)
    {
        if (TabWidgetClass->objectName().isEmpty())
            TabWidgetClass->setObjectName(QString::fromUtf8("TabWidgetClass"));
        TabWidgetClass->resize(689, 621);
        verticalLayout_2 = new QVBoxLayout(TabWidgetClass);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        btnOpen = new QPushButton(TabWidgetClass);
        btnOpen->setObjectName(QString::fromUtf8("btnOpen"));

        verticalLayout_2->addWidget(btnOpen);

        widget = new QWidget(TabWidgetClass);
        widget->setObjectName(QString::fromUtf8("widget"));

        verticalLayout_2->addWidget(widget);


        retranslateUi(TabWidgetClass);

        QMetaObject::connectSlotsByName(TabWidgetClass);
    } // setupUi

    void retranslateUi(QWidget *TabWidgetClass)
    {
        TabWidgetClass->setWindowTitle(QApplication::translate("TabWidgetClass", "TabWidget", nullptr));
        btnOpen->setText(QApplication::translate("TabWidgetClass", "Open", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TabWidgetClass: public Ui_TabWidgetClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABWIDGET_H
