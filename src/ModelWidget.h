#pragma once
#include <QWidget>
#include <QDragMoveEvent>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QDragLeaveEvent>
#include <QPaintEvent>
class modelWidget : public QWidget
{ 
public:
	modelWidget(QWidget* parent = nullptr);
	~modelWidget();

	void openFile(const QString& filePath);
	void paintEvent(QPaintEvent* event) override;
	void resizeEvent(QResizeEvent* event) override;

private:
	QString filePath;
};

