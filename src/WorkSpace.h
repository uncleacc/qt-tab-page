#pragma once
#include <QWidget>
#include <QTabWidget>
#include <QStackedWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include "mySplitter.h"
#include "config.h"
#include "tabPages.h"
#include "myTabBar.h" 
#include "modelWidget.h"

class WorkSpace: public QWidget
{
public:
    WorkSpace(QWidget* parent = nullptr);
    ~WorkSpace();

    bool OpenFile(const QString &filePath) const;

public slots:
    void slot_FullScreen(modelWidget* src);
    void slot_restore();

private:
    mySplitter* container;
    QStackedWidget* stkWidget;
    QWidget* whiteBoard;
    QVBoxLayout* whiteBoard_layout;
    QPushButton* closeBtn;
    int oriIndex;
};

