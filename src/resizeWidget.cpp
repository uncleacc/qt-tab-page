#include "resizeWidget.h"
#include <QHBoxLayout>
#include <QDebug>

resizeWidget::resizeWidget(QWidget* parent, QWidget* child): QWidget(parent)
{
	QHBoxLayout* layout = new QHBoxLayout(this);
	layout->addWidget(child);
	layout->setContentsMargins(0, 10, 0, 0);
	setLayout(layout);
}

resizeWidget::~resizeWidget()
{
}

void resizeWidget::resizeEvent(QResizeEvent* event)
{
	if (width() == height()) return;
	int mi = std::min(width(), height());
	resize(mi, mi);
	move((((QWidget*)parent())->width() - mi) / 2, (((QWidget*)parent())->height() - mi) / 2);
}


