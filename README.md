# Qt-TabPage

#### 介绍
Qt小项目，实现类似vscode、matlab的分屏显示效果

#### 使用说明

1. 下载Qt目录
2. 下载CMake
3. 打开CMakeLists.txt
   1. 更改`set(Qt5_DIR E:\\Qt\\5.12.6\\msvc2017_64\\lib\\cmake\\Qt5)`后的目录为你的Qt5安装目录
4. 用CMake生成编译即可

#### 运行截图

![image-20230712151450397](https://pic.imgdb.cn/item/64ae65a11ddac507ccfd50f1.png)